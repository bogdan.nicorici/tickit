# tickit plugin for Craft CMS 3.x

Ticketing support system

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project
        
2. Open the `composer.json` file and add the following inside your `repositories` section:
        
        {
            "type": "package",
            "package": {
                "name": "teamextension/tickit",
                "version": "1.0.0",
                "type": "craft-plugin",
                "source": {
                    "url": "https://gitlab.com/armchairpartners/tickit.git",
                    "type": "git",
                    "reference": "master"
                }
            }
        }
        

3. Then tell Composer to load the plugin:

        composer require teamextension/tickit

4. In the Control Panel, go to Settings → Plugins and click the “Install” button for tickit.

## tickit Overview

-Insert text here-

## Configuring tickit

-Insert text here-

## Using tickit

-Insert text here-

## tickit Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Bogdan Nicorici @ TeamExtension](http://teamextension.ro)
