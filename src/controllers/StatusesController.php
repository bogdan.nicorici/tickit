<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\controllers;

use teamextension\tickit\services\StatusesService;
use teamextension\tickit\Tickit;

use craft\web\Controller;
use teamextension\tickit\models\Status;
use yii\web\HttpException;
use yii\web\Response;

/**
 * StatusesService Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property \teamextension\tickit\services\StatusesService $statusesService
 */
class StatusesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/tickit/statuses
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->renderTemplate('tickit/statuses');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSave(): Response
    {
        $post = \Craft::$app->request->post();

        $statusId = $post['statusId'] ?? null;

        $status = $this->getNewOrExistingStatus($statusId);

        $status->setAttributes($post);

        if($this->getStatusesService()->save($status))
        {
            // Return JSON response if the request is an AJAX request
            if (\Craft::$app->request->isAjax)
            {
                return $this->asJson(['success' => true]);
            }

            \Craft::$app->session->setNotice(Tickit::t('Status saved'));
            \Craft::$app->session->setFlash(Tickit::t('Status saved'));

            return $this->redirectToPostedUrl($status);
        }

        // Return JSON response if the request is an AJAX request
        if (\Craft::$app->request->isAjax)
        {
            return $this->asJson(['success' => false]);
        }

        \Craft::$app->session->setError(Tickit::t('Status not saved'));

        \Craft::$app->urlManager->setRouteParams(['status' => $status, 'errors' => $status->getErrors()]);

        //TODO: This looks kinda dodgy
        return $this->renderEditForm($status, 'Create new status');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionCreate(): Response
    {
        $post = \Craft::$app->request->post();

        $statusId = $post['statusId'] ?? null;

        $status = $this->getNewOrExistingStatus($statusId);

        $status->setAttributes($post);

        return $this->renderEditForm($status, 'Create new status');
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionEdit(int $id): Response
    {
        $model = $this->getStatusesService()->getStatusById($id);

        if (!$model)
        {
            throw new \HttpException(404, Tickit::t('"Status with ID {id} not found', ['id' => $id]));
        }

        return $this->renderEditForm($model, $model->name);
    }

    /**
     * @return Response
     * @throws \yii\db\Exception
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws HttpException
     */
    public function actionDelete()
    {
        $this->requirePostRequest();
        if (!\Craft::$app->request->isAjax)
        {
            throw new HttpException(404, 'Page does not exist');
        }

        $statusId = \Craft::$app->request->post('id');
//        $this->getStatusesService()->deleteById($statusId);
        return $this->asJson(['success' => $this->getStatusesService()->deleteById($statusId)]);
    }

    /**
     * @param Status $model
     * @param string $title
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     */
    private function renderEditForm(Status $model, string $title): Response
    {
        $variables = [
            'status'             => $model,
            'title'              => $title,
            'continueEditingUrl' => 'tickit/statuses/{id}',
        ];

        return $this->renderTemplate('tickit/statuses/_edit', $variables);
    }

    /**
     * @return StatusesService
     */
    protected function getStatusesService(): StatusesService
    {
        return Tickit::getInstance()->statuses;
    }

    /**
     * @param $statusId
     * @return Status
     * @throws \HttpException
     */
    private function getNewOrExistingStatus($statusId): Status
    {
        $statusService = $this->getStatusesService();

        if($statusId)
        {
            $status = $statusService->getStatusById($statusId);

            if(!$status)
            {
                throw new \HttpException(404, Tickit::t('Status with ID {id} not found', ['id' => $statusId]));
            }
        }
        else
        {
            $status = Status::create();
        }

        return $status;
    }

    /**
     * @return array|bool
     */
    public function getAllowAnonymous()
    {
        return $this->allowAnonymous;
    }

    /**
     * @param array|bool $allowAnonymous
     */
    public function setAllowAnonymous($allowAnonymous)
    {
        $this->allowAnonymous = $allowAnonymous;
    }
}
