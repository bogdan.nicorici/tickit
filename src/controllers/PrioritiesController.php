<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\controllers;

use teamextension\tickit\services\PrioritiesService;
use teamextension\tickit\Tickit;

use craft\web\Controller;
use teamextension\tickit\models\Priority;
use yii\web\HttpException;
use yii\web\Response;

/**
 * PrioritiesService Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property \teamextension\tickit\services\PrioritiesService $prioritiesService
 */
class PrioritiesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/tickit/priorities
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->renderTemplate('tickit/priorities');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSave(): Response
    {
        $post = \Craft::$app->request->post();

        $priorityId = $post['priorityId'] ?? null;

        $priority = $this->getNewOrExistingPriority($priorityId);

        $priority->setAttributes($post);

        if($this->getPrioritiesService()->save($priority))
        {
            // Return JSON response if the request is an AJAX request
            if (\Craft::$app->request->isAjax)
            {
                return $this->asJson(['success' => true]);
            }

            \Craft::$app->session->setNotice(Tickit::t('Priority saved'));
            \Craft::$app->session->setFlash(Tickit::t('Priority saved'));

            return $this->redirectToPostedUrl($priority);
        }

        // Return JSON response if the request is an AJAX request
        if (\Craft::$app->request->isAjax)
        {
            return $this->asJson(['success' => false]);
        }

        \Craft::$app->session->setError(Tickit::t('Priority not saved'));

        \Craft::$app->urlManager->setRouteParams(['priority' => $priority, 'errors' => $priority->getErrors()]);

        //TODO: This looks kinda dodgy
        return $this->renderEditForm($priority, 'Create new priority');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionCreate(): Response
    {
        $post = \Craft::$app->request->post();

        $priorityId = $post['priorityId'] ?? null;

        $priority = $this->getNewOrExistingPriority($priorityId);

        $priority->setAttributes($post);

        return $this->renderEditForm($priority, 'Create new priority');
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionEdit(int $id): Response
    {
        $model = $this->getPrioritiesService()->getPriorityById($id);

        if (!$model)
        {
            throw new \HttpException(404, Tickit::t('"Priority with ID {id} not found', ['id' => $id]));
        }

        return $this->renderEditForm($model, $model->name);
    }

    /**
     * @return Response
     * @throws \yii\db\Exception
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws HttpException
     */
    public function actionDelete()
    {
        $this->requirePostRequest();
        if (!\Craft::$app->request->isAjax)
        {
            throw new HttpException(404, 'Page does not exist');
        }

        $priorityId = \Craft::$app->request->post('id');
//        $this->getPrioritiesService()->deleteById($priorityId);
        return $this->asJson(['success' => $this->getPrioritiesService()->deleteById($priorityId)]);
    }

    /**
     * @param Priority $model
     * @param string $title
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     */
    private function renderEditForm(Priority $model, string $title): Response
    {
        $variables = [
            'priority'             => $model,
            'title'              => $title,
            'continueEditingUrl' => 'tickit/priorities/{id}',
        ];

        return $this->renderTemplate('tickit/priorities/_edit', $variables);
    }

    /**
     * @return PrioritiesService
     */
    protected function getPrioritiesService(): PrioritiesService
    {
        return Tickit::getInstance()->priorities;
    }

    /**
     * @param $priorityId
     * @return Priority
     * @throws \HttpException
     */
    private function getNewOrExistingPriority($priorityId): Priority
    {
        $priorityService = $this->getPrioritiesService();

        if($priorityId)
        {
            $priority = $priorityService->getPriorityById($priorityId);

            if(!$priority)
            {
                throw new \HttpException(404, Tickit::t('Priority with ID {id} not found', ['id' => $priorityId]));
            }
        }
        else
        {
            $priority = Priority::create();
        }

        return $priority;
    }

    /**
     * @return array|bool
     */
    public function getAllowAnonymous()
    {
        return $this->allowAnonymous;
    }

    /**
     * @param array|bool $allowAnonymous
     */
    public function setAllowAnonymous($allowAnonymous)
    {
        $this->allowAnonymous = $allowAnonymous;
    }
}
