<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\controllers;

use teamextension\tickit\services\CategoriesService;
use teamextension\tickit\Tickit;

use craft\web\Controller;
use teamextension\tickit\models\Category;
use yii\web\HttpException;
use yii\web\Response;

/**
 * CategoriesService Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property \teamextension\tickit\services\CategoriesService $categoriesService
 */
class CategoriesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/tickit/categories
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->renderTemplate('tickit/categories');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSave(): Response
    {
        $post = \Craft::$app->request->post();

        $categoryId = $post['categoryId'] ?? null;

        $category = $this->getNewOrExistingCategory($categoryId);

        $category->setAttributes($post);

        if($this->getCategoriesService()->save($category))
        {
            // Return JSON response if the request is an AJAX request
            if (\Craft::$app->request->isAjax)
            {
                return $this->asJson(['success' => true]);
            }

            \Craft::$app->session->setNotice(Tickit::t('Category saved'));
            \Craft::$app->session->setFlash(Tickit::t('Category saved'));

            return $this->redirectToPostedUrl($category);
        }

        // Return JSON response if the request is an AJAX request
        if (\Craft::$app->request->isAjax)
        {
            return $this->asJson(['success' => false]);
        }

        \Craft::$app->session->setError(Tickit::t('Category not saved'));

        \Craft::$app->urlManager->setRouteParams(['category' => $category, 'errors' => $category->getErrors()]);

        //TODO: This looks kinda dodgy
        return $this->renderEditForm($category, 'Create new category');
    }

    /**
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionCreate(): Response
    {
        $post = \Craft::$app->request->post();

        $categoryId = $post['categoryId'] ?? null;

        $category = $this->getNewOrExistingCategory($categoryId);

        $category->setAttributes($post);

        return $this->renderEditForm($category, 'Create new category');
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     * @throws \HttpException
     */
    public function actionEdit(int $id): Response
    {
        $model = $this->getCategoriesService()->getCategoryById($id);

        if (!$model)
        {
            throw new \HttpException(404, Tickit::t('"Category with ID {id} not found', ['id' => $id]));
        }

        return $this->renderEditForm($model, $model->name);
    }

    /**
     * @return Response
     * @throws \yii\db\Exception
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws HttpException
     */
    public function actionDelete()
    {
        $this->requirePostRequest();
        if (!\Craft::$app->request->isAjax)
        {
            throw new HttpException(404, 'Page does not exist');
        }

        $categoryId = \Craft::$app->request->post('id');
//        $this->getCategoriesService()->deleteById($categoryId);
        return $this->asJson(['success' => $this->getCategoriesService()->deleteById($categoryId)]);
    }

    /**
     * @param Category $model
     * @param string $title
     *
     * @return Response
     * @throws \yii\base\InvalidParamException
     */
    private function renderEditForm(Category $model, string $title): Response
    {
        $variables = [
            'category'             => $model,
            'title'              => $title,
            'continueEditingUrl' => 'tickit/categories/{id}',
        ];

        return $this->renderTemplate('tickit/categories/_edit', $variables);
    }

    /**
     * @return CategoriesService
     */
    protected function getCategoriesService(): CategoriesService
    {
        return Tickit::getInstance()->categories;
    }

    /**
     * @param $categoryId
     * @return Category
     * @throws \HttpException
     */
    private function getNewOrExistingCategory($categoryId): Category
    {
        $categoryService = $this->getCategoriesService();

        if($categoryId)
        {
            $category = $categoryService->getCategoryById($categoryId);

            if(!$category)
            {
                throw new \HttpException(404, Tickit::t('Category with ID {id} not found', ['id' => $categoryId]));
            }
        }
        else
        {
            $category = Category::create();
        }

        return $category;
    }

    /**
     * @return array|bool
     */
    public function getAllowAnonymous()
    {
        return $this->allowAnonymous;
    }

    /**
     * @param array|bool $allowAnonymous
     */
    public function setAllowAnonymous($allowAnonymous)
    {
        $this->allowAnonymous = $allowAnonymous;
    }
}
