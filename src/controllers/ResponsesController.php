<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\controllers;

use teamextension\tickit\Tickit;

use Craft;
use craft\web\Controller;

/**
 * ResponsesService Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class ResponsesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/tickit/responses
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the ResponsesController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/tickit/responses/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the ResponsesController actionDoSomething() method';

        return $result;
    }
}
