/**
 * tickit plugin for Craft CMS
 *
 * tickit JS
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 * @link      http://teamextension.ro
 * @package   Tickit
 * @since     1.0.0
 */
