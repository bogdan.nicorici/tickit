<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\services;

use teamextension\tickit\Events\Categories\DeleteEvent;
use teamextension\tickit\Events\Categories\SaveEvent;
use teamextension\tickit\models\Category;

use craft\base\Component;
use teamextension\tickit\records\CategoryRecord;

/**
 * CategoriesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property null $defaultCategory
 */
class CategoriesService extends Component
{
    const EVENT_BEFORE_SAVE   = 'beforeSave';
    const EVENT_AFTER_SAVE    = 'afterSave';
    const EVENT_BEFORE_DELETE = 'beforeDelete';
    const EVENT_AFTER_DELETE  = 'afterDelete';

    /** @var CategoryRecord[] */
    private static $categoryCache;
    private static $allCategoriesLoaded;

    // Public Methods
    // =========================================================================

    public function getDefaultCategory()
    {
        $record = CategoryRecord::find()
            ->where(['isDefault' => true])
            ->one();

        return $this->createCategoryFromRecord($record);
    }

    public function getAllCategories($indexedById = true): array
    {
        if (null === self::$categoryCache || !self::$allCategoriesLoaded)
        {
            self::$categoryCache = [];
            $results = CategoryRecord::find()->all();

            foreach($results as $result)
            {
                $category = $this->createCategoryFromRecord($result);
                if($category)
                {
                    self::$categoryCache[$category->id] = $category;
                }
            }

            self::$allCategoriesLoaded = true;
        }

        if(!$indexedById)
        {
            return array_values(self::$categoryCache);
        }

        return self::$categoryCache;
    }

    /**
     * @param null $id
     * @return null|CategoryRecord
     */
    public function getCategoryById($id = null)
    {
        if(null === $id)
        {
            return null;
        }

        if (null === self::$categoryCache || !isset(self::$categoryCache[$id]))
        {
            $record = CategoryRecord::findOne(['id' => $id]);
            $category = null;
            if($record)
            {
                $category = $this->createCategoryFromRecord($record);
            }
            self::$categoryCache[$id] = $category;
        }
        return self::$categoryCache[$id];
    }

    /**
     * @param null $id
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function deleteById($id = null): bool
    {
        $model = $this->getCategoryById($id);

        if(!$model || $model->isDefault)
        {
            return false;
        }

        $record = CategoryRecord::findOne(['id' => $model->id]);
        if(!$record)
        {
            return false;
        }

        $beforeDeleteEvent = new DeleteEvent($model);
        $this->trigger(self::EVENT_BEFORE_DELETE, $beforeDeleteEvent);
        if(!$beforeDeleteEvent->isValid)
        {
            return false;
        }

        $transaction = \Craft::$app->getDb()->beginTransaction();
        try
        {
            $affectedRows = \Craft::$app
                ->getDb()
                ->createCommand()
                ->delete(CategoryRecord::TABLE, ['id' => $record->id])
                ->execute();

            if ($transaction !== null)
            {
                $transaction->commit();
            }

            $this->trigger(self::EVENT_AFTER_DELETE, new DeleteEvent($model));

            return (bool) $affectedRows;
        }
        catch(\Exception $e)
        {
            if ($transaction !== null)
            {
                $transaction->rollBack();
            }
            throw $e;
        }
    }

    /**
     * @param Category $model
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function save(Category $model): bool
    {
        $isNew = !$model->id;

        if(!$isNew)
        {
            $record = CategoryRecord::findOne(['id' => $model->id]);
        }
        else
        {
            $record = CategoryRecord::create();
        }

        $record->name       = $model->name;
        $record->color      = $model->color;
        $record->handle     = $model->handle;
        $record->isDefault  = (bool) $model->isDefault;

        $record->validate();

        $model->addErrors($record->getErrors());

        $beforeSaveEvent = new SaveEvent($model, $isNew);
        $this->trigger(self::EVENT_BEFORE_SAVE, $beforeSaveEvent);

        if($beforeSaveEvent->isValid && !$model->hasErrors())
        {
            $transaction = \Craft::$app->getDb()->beginTransaction();
            try
            {
                $record->save(false);

                self::$categoryCache[$record->id] = $record;

                if ($transaction !== null)
                {
                    $transaction->commit();
                }

                if ($record->isDefault)
                {
                    \Craft::$app
                        ->getDb()
                        ->createCommand()
                        ->update(
                            CategoryRecord::TABLE,
                            ['isDefault' => 0],
                            'id != :id',
                            ['id' => $record->id]
                        )
                        ->execute();
                }

                $this->trigger(self::EVENT_AFTER_SAVE, new SaveEvent($model, $isNew));
                return true;
            }
            catch(\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollBack();
                }
                throw $e;
            }
        }

        return false;
    }

    private function createCategoryFromRecord(CategoryRecord $record = null)
    {
        if(!$record)
        {
            return null;
        }

        return new Category($record->toArray([
            'id',
            'name',
            'handle',
            'color',
            'isDefault',
            'dateCreated',
            'dateUpdated',
        ])) ?: null;
    }
}
