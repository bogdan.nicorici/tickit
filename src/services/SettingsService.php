<?php
namespace teamextension\tickit\services;

use teamextension\tickit\Tickit;
use teamextension\tickit\models\Settings as SettingsModel;
use yii\base\Component;

/**
 *
 * @property \teamextension\tickit\services\SettingsService|\teamextension\tickit\models\Settings $settingsModel
 */
class SettingsService extends Component
{
    /** @var SettingsService */
    private static $settingsModel;

    /**
     * @return SettingsService
     */
    public function getSettingsModel(): SettingsModel
    {
        if (null === self::$settingsModel)
        {
            $plugin              = Tickit::getInstance();
            self::$settingsModel = $plugin->getSettings();
        }

        return self::$settingsModel;
    }
}
