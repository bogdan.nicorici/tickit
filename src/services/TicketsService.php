<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\services;

use teamextension\tickit\elements\Ticket;
use teamextension\tickit\records\TicketRecord;
use teamextension\tickit\Tickit;

use craft\base\Component;

/**
 * Tickets Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class TicketsService extends Component
{
    // Public Methods
    // =========================================================================

    private static $ticketCache = [];

    public function getTicketById($id)
    {
        if(null === self::$ticketCache || !isset(self::$ticketCache[$id]))
        {
            if (null === self::$ticketCache)
            {
                self::$ticketCache = [];
            }

            self::$ticketCache[$id] = Ticket::find()->id($id)->one();
        }

        return self::$ticketCache[$id];
    }

    /**
     * @param int $oldStatusId
     * @param int $newStatusId
     */
    public function swapStatuses($oldStatusId, $newStatusId)
    {
        $oldStatusId = (int) $oldStatusId;
        $newStatusId = (int) $newStatusId;
        \Craft::$app
            ->db
            ->createCommand()
            ->update(
                TicketRecord::TABLE,
                ['statusId' => $newStatusId],
                'statusId = :oldStatusId',
                [
                    'oldStatusId' => $oldStatusId,
                ]
            );
    }

    /**
     * @param int $oldPriorityId
     * @param int $newPriorityId
     */
    public function swapPriorities($oldPriorityId, $newPriorityId)
    {
        $oldPriorityId = (int) $oldPriorityId;
        $newPriorityId = (int) $newPriorityId;
        \Craft::$app
            ->db
            ->createCommand()
            ->update(
                TicketRecord::TABLE,
                ['priorityId' => $newPriorityId],
                'priorityId = :oldPriorityId',
                [
                    'oldPriorityId' => $oldPriorityId,
                ]
            );
    }

    /**
     * @param int $oldCategoryId
     * @param int $newCategoryId
     */
    public function swapCategories($oldCategoryId, $newCategoryId)
    {
        $oldCategoryId = (int) $oldCategoryId;
        $newCategoryId = (int) $newCategoryId;
        \Craft::$app
            ->db
            ->createCommand()
            ->update(
                TicketRecord::TABLE,
                ['categoryId' => $newCategoryId],
                'categoryId = :oldCategoryId',
                [
                    'oldCategoryId' => $oldCategoryId,
                ]
            );
    }
}
