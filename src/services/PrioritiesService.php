<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\services;

use teamextension\tickit\Events\Priorities\DeleteEvent;
use teamextension\tickit\Events\Priorities\SaveEvent;
use teamextension\tickit\models\Priority;

use craft\base\Component;
use teamextension\tickit\records\PriorityRecord;

/**
 * PrioritiesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property null $defaultPriority
 */
class PrioritiesService extends Component
{
    const EVENT_BEFORE_SAVE   = 'beforeSave';
    const EVENT_AFTER_SAVE    = 'afterSave';
    const EVENT_BEFORE_DELETE = 'beforeDelete';
    const EVENT_AFTER_DELETE  = 'afterDelete';

    /** @var PriorityRecord[] */
    private static $priorityCache;
    private static $allPrioritiesLoaded;

    // Public Methods
    // =========================================================================

    public function getDefaultPriority()
    {
        $record = PriorityRecord::find()
            ->where(['isDefault' => true])
            ->one();

        return $this->createPriorityFromRecord($record);
    }

    public function getAllPriorities($indexedById = true): array
    {
        if (null === self::$priorityCache || !self::$allPrioritiesLoaded)
        {
            self::$priorityCache = [];
            $results = PriorityRecord::find()->all();

            foreach($results as $result)
            {
                $priority = $this->createPriorityFromRecord($result);
                if($priority)
                {
                    self::$priorityCache[$priority->id] = $priority;
                }
            }

            self::$allPrioritiesLoaded = true;
        }

        if(!$indexedById)
        {
            return array_values(self::$priorityCache);
        }

        return self::$priorityCache;
    }

    /**
     * @param null $id
     * @return null|PriorityRecord
     */
    public function getPriorityById($id = null)
    {
        if(null === $id)
        {
            return null;
        }

        if (null === self::$priorityCache || !isset(self::$priorityCache[$id]))
        {
            $record = PriorityRecord::findOne(['id' => $id]);
            $priority = null;
            if($record)
            {
                $priority = $this->createPriorityFromRecord($record);
            }
            self::$priorityCache[$id] = $priority;
        }
        return self::$priorityCache[$id];
    }

    /**
     * @param null $id
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function deleteById($id = null): bool
    {
        $model = $this->getPriorityById($id);

        if(!$model || $model->isDefault)
        {
            return false;
        }

        $record = PriorityRecord::findOne(['id' => $model->id]);
        if(!$record)
        {
            return false;
        }

        $beforeDeleteEvent = new DeleteEvent($model);
        $this->trigger(self::EVENT_BEFORE_DELETE, $beforeDeleteEvent);
        if(!$beforeDeleteEvent->isValid)
        {
            return false;
        }

        $transaction = \Craft::$app->getDb()->beginTransaction();
        try
        {
            $affectedRows = \Craft::$app
                ->getDb()
                ->createCommand()
                ->delete(PriorityRecord::TABLE, ['id' => $record->id])
                ->execute();

            if ($transaction !== null)
            {
                $transaction->commit();
            }

            $this->trigger(self::EVENT_AFTER_DELETE, new DeleteEvent($model));

            return (bool) $affectedRows;
        }
        catch(\Exception $e)
        {
            if ($transaction !== null)
            {
                $transaction->rollBack();
            }
            throw $e;
        }
    }

    /**
     * @param Priority $model
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function save(Priority $model): bool
    {
        $isNew = !$model->id;

        if(!$isNew)
        {
            $record = PriorityRecord::findOne(['id' => $model->id]);
        }
        else
        {
            $record = PriorityRecord::create();
        }

        $record->name       = $model->name;
        $record->color      = $model->color;
        $record->handle     = $model->handle;
        $record->isDefault  = (bool) $model->isDefault;

        $record->validate();

        $model->addErrors($record->getErrors());

        $beforeSaveEvent = new SaveEvent($model, $isNew);
        $this->trigger(self::EVENT_BEFORE_SAVE, $beforeSaveEvent);

        if($beforeSaveEvent->isValid && !$model->hasErrors())
        {
            $transaction = \Craft::$app->getDb()->beginTransaction();
            try
            {
                $record->save(false);

                self::$priorityCache[$record->id] = $record;

                if ($transaction !== null)
                {
                    $transaction->commit();
                }

                if ($record->isDefault)
                {
                    \Craft::$app
                        ->getDb()
                        ->createCommand()
                        ->update(
                            PriorityRecord::TABLE,
                            ['isDefault' => 0],
                            'id != :id',
                            ['id' => $record->id]
                        )
                        ->execute();
                }

                $this->trigger(self::EVENT_AFTER_SAVE, new SaveEvent($model, $isNew));
                return true;
            }
            catch(\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollBack();
                }
                throw $e;
            }
        }

        return false;
    }

    private function createPriorityFromRecord(PriorityRecord $record = null)
    {
        if(!$record)
        {
            return null;
        }

        return new Priority($record->toArray([
            'id',
            'name',
            'handle',
            'color',
            'isDefault',
            'dateCreated',
            'dateUpdated',
        ])) ?: null;
    }
}
