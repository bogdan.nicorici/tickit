<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\services;

use teamextension\tickit\Events\Statuses\DeleteEvent;
use teamextension\tickit\Events\Statuses\SaveEvent;
use teamextension\tickit\models\Status;

use craft\base\Component;
use teamextension\tickit\records\StatusRecord;

/**
 * StatusesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property null $defaultStatus
 */
class StatusesService extends Component
{
    const EVENT_BEFORE_SAVE   = 'beforeSave';
    const EVENT_AFTER_SAVE    = 'afterSave';
    const EVENT_BEFORE_DELETE = 'beforeDelete';
    const EVENT_AFTER_DELETE  = 'afterDelete';

    /** @var StatusRecord[] */
    private static $statusCache;
    private static $allStatusesLoaded;

    // Public Methods
    // =========================================================================

    public function getDefaultStatus()
    {
        $record = StatusRecord::find()
            ->where(['isDefault' => true])
            ->one();

        return $this->createStatusFromRecord($record);
    }

    public function getAllStatuses($indexedById = true): array
    {
        if (null === self::$statusCache || !self::$allStatusesLoaded)
        {
            self::$statusCache = [];
            $results = StatusRecord::find()->all();

            foreach($results as $result)
            {
                $status = $this->createStatusFromRecord($result);
                if($status)
                {
                    self::$statusCache[$status->id] = $status;
                }
            }

            self::$allStatusesLoaded = true;
        }

        if(!$indexedById)
        {
            return array_values(self::$statusCache);
        }

        return self::$statusCache;
    }

    /**
     * @param null $id
     * @return null|StatusRecord
     */
    public function getStatusById($id = null)
    {
        if(null === $id)
        {
            return null;
        }

        if (null === self::$statusCache || !isset(self::$statusCache[$id]))
        {
            $record = StatusRecord::findOne(['id' => $id]);
            $status = null;
            if($record)
            {
                $status = $this->createStatusFromRecord($record);
            }
            self::$statusCache[$id] = $status;
        }
        return self::$statusCache[$id];
    }

    /**
     * @param null $handle
     * @return null|StatusRecord
     */
    public function getStatusByHandle($handle = null)
    {
        if(null === $handle)
        {
            return null;
        }

        $record = StatusRecord::findOne(['handle' => $handle]);

        if($record)
        {
            return $this->createStatusFromRecord($record);
        }

        return null;
    }

    /**
     * @param null $id
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function deleteById($id = null): bool
    {
        $model = $this->getStatusById($id);

        if(!$model || $model->isDefault)
        {
            return false;
        }

        $record = StatusRecord::findOne(['id' => $model->id]);
        if(!$record)
        {
            return false;
        }

        $beforeDeleteEvent = new DeleteEvent($model);
        $this->trigger(self::EVENT_BEFORE_DELETE, $beforeDeleteEvent);
        if(!$beforeDeleteEvent->isValid)
        {
            return false;
        }

        $transaction = \Craft::$app->getDb()->beginTransaction();
        try
        {
            $affectedRows = \Craft::$app
                ->getDb()
                ->createCommand()
                ->delete(StatusRecord::TABLE, ['id' => $record->id])
                ->execute();

            if ($transaction !== null)
            {
                $transaction->commit();
            }

            $this->trigger(self::EVENT_AFTER_DELETE, new DeleteEvent($model));

            return (bool) $affectedRows;
        }
        catch(\Exception $e)
        {
            if ($transaction !== null)
            {
                $transaction->rollBack();
            }
            throw $e;
        }
    }

    /**
     * @param Status $model
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function save(Status $model): bool
    {
        $isNew = !$model->id;

        if(!$isNew)
        {
            $record = StatusRecord::findOne(['id' => $model->id]);
        }
        else
        {
            $record = StatusRecord::create();
        }

        $record->name       = $model->name;
        $record->color      = $model->color;
        $record->handle     = $model->handle;
        $record->isDefault  = (bool) $model->isDefault;

        $record->validate();

        $model->addErrors($record->getErrors());

        $beforeSaveEvent = new SaveEvent($model, $isNew);
        $this->trigger(self::EVENT_BEFORE_SAVE, $beforeSaveEvent);

        if($beforeSaveEvent->isValid && !$model->hasErrors())
        {
            $transaction = \Craft::$app->getDb()->beginTransaction();
            try
            {
                $record->save(false);

                self::$statusCache[$record->id] = $record;

                if ($transaction !== null)
                {
                    $transaction->commit();
                }

                if ($record->isDefault)
                {
                    \Craft::$app
                        ->getDb()
                        ->createCommand()
                        ->update(
                            StatusRecord::TABLE,
                            ['isDefault' => 0],
                            'id != :id',
                            ['id' => $record->id]
                        )
                        ->execute();
                }

                $this->trigger(self::EVENT_AFTER_SAVE, new SaveEvent($model, $isNew));
                return true;
            }
            catch(\Exception $e)
            {
                if ($transaction !== null)
                {
                    $transaction->rollBack();
                }
                throw $e;
            }
        }

        return false;
    }

    private function createStatusFromRecord(StatusRecord $record = null)
    {
        if(!$record)
        {
            return null;
        }

        return new Status($record->toArray([
            'id',
            'name',
            'handle',
            'color',
            'isDefault',
            'dateCreated',
            'dateUpdated',
        ])) ?: null;
    }
}
