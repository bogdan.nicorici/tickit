<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\migrations;

use Craft;
use craft\config\DbConfig;
use craft\db\Migration;
use teamextension\tickit\records\CategoryRecord;
use teamextension\tickit\records\PriorityRecord;
use teamextension\tickit\records\ReplyRecord;
use teamextension\tickit\records\StatusRecord;
use teamextension\tickit\records\TicketRecord;

/**
 * tickit Install Migration
 *
 * If your plugin needs to create any custom database tables when it gets installed,
 * create a migrations/ folder within your plugin folder, and save an Install.php file
 * within it using the following template:
 *
 * If you need to perform any additional actions on install/uninstall, override the
 * safeUp() and safeDown() methods.
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class Install extends Migration
{
    // Public Properties
    // =========================================================================

    /**
     * @var string The database driver to use
     */
    public $driver;

    // Public Methods
    // =========================================================================

    /**
     * This method contains the logic to be executed when applying this migration.
     * This method differs from [[up()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[up()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeUp()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        if ($this->createTables()) {
            $this->createIndexes();
            $this->addForeignKeys();
            // Refresh the db schema caches
            Craft::$app->db->schema->refresh();
            $this->insertDefaultData();
        }

        return true;
    }

    /**
     * Creates the tables needed for the Records used by the plugin
     *
     * @return bool
     */
    protected function createTables()
    {
        $tablesCreated = false;

        // tickit_tickets table
        $tableSchema = Craft::$app->db->schema->getTableSchema(TicketRecord::TABLE);
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                TicketRecord::TABLE,
                [
                    'id' => $this->primaryKey(),
                    'number' => $this->string(255),
                    'statusId' => $this->integer()->notNull(),
                    'priorityId' => $this->integer()->notNull(),
                    'categoryId' => $this->integer()->defaultValue(0),
                    'userId'    => $this->integer()->notNull()->defaultValue(1),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                    // Custom columns in the table
                    'siteId' => $this->integer()->notNull(),
                ]
            );
        }

        // tickit_question table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%tickit_questions}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%tickit_question}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                    // Custom columns in the table
                    'siteId' => $this->integer()->notNull(),
                    'some_field' => $this->string(255)->notNull()->defaultValue(''),
                ]
            );
        }

        // tickit_categories table
        $tableSchema = Craft::$app->db->schema->getTableSchema(CategoryRecord::TABLE);
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                CategoryRecord::TABLE,
                [
                    'id' => $this->primaryKey(),
                    'name' => $this->string(255)->notNull(),
                    'color' => $this->string(10)->notNull()->defaultValue('#007777'),
                    'handle' => $this->string(255)->notNull(),
                    'isDefault' => $this->boolean()->notNull()->defaultValue(false),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        // tickit_statuses table
        $tableSchema = Craft::$app->db->schema->getTableSchema(StatusRecord::TABLE);
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                StatusRecord::TABLE,
                [
                    'id' => $this->primaryKey(),
                    'name' => $this->string(255)->notNull(),
                    'color' => $this->string(10)->notNull()->defaultValue('#007777'),
                    'handle' => $this->string(255)->notNull(),
                    'isDefault' => $this->boolean()->notNull()->defaultValue(false),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        // tickit_priorities table
        $tableSchema = Craft::$app->db->schema->getTableSchema(PriorityRecord::TABLE);
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                PriorityRecord::TABLE,
                [
                    'id' => $this->primaryKey(),
                    'name' => $this->string(255)->notNull(),
                    'color' => $this->string(10)->notNull()->defaultValue('#007777'),
                    'handle' => $this->string(255)->notNull(),
                    'isDefault' => $this->boolean()->notNull()->defaultValue(false),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        // tickit_template table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%tickit_templates}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%tickit_template}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                    // Custom columns in the table
                    'siteId' => $this->integer()->notNull(),
                    'some_field' => $this->string(255)->notNull()->defaultValue(''),
                ]
            );
        }

        // tickit_replies table
        $tableSchema = Craft::$app->db->schema->getTableSchema(ReplyRecord::TABLE);
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                ReplyRecord::TABLE,
                [
                    'id' => $this->primaryKey(),
                    'userId' => $this->integer()->notNull()->defaultValue(1),
                    'message' => $this->longText(),
                    'ticketId' => $this->integer()->notNull()->defaultValue(2),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        return $tablesCreated;
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates the indexes needed for the Records used by the plugin
     *
     * @return void
     */
    protected function createIndexes()
    {
        // tickit_ticket table
        $this->createIndex(
            $this->db->getIndexName(
                TicketRecord::TABLE,
                'number',
                true
            ),
            TicketRecord::TABLE,
            'number',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

        // tickit_question table
        $this->createIndex(
            $this->db->getIndexName(
                '{{%tickit_question}}',
                'some_field',
                true
            ),
            '{{%tickit_question}}',
            'some_field',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

        // tickit_category table
        $this->createIndex(
            $this->db->getIndexName(
                CategoryRecord::TABLE,
                'handle',
                true
            ),
            CategoryRecord::TABLE,
            'handle',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

        // tickit_status table
        $this->createIndex(
            $this->db->getIndexName(
                StatusRecord::TABLE,
                'handle',
                true
            ),
            StatusRecord::TABLE,
            'handle',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

        // tickit_priority table
        $this->createIndex(
            $this->db->getIndexName(
                PriorityRecord::TABLE,
                'handle',
                true
            ),
            PriorityRecord::TABLE,
            'handle',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

        // tickit_template table
        $this->createIndex(
            $this->db->getIndexName(
                '{{%tickit_template}}',
                'some_field',
                true
            ),
            '{{%tickit_template}}',
            'some_field',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

//        // tickit_reply table
//        $this->createIndex(
//            $this->db->getIndexName(
//                ReplyRecord::TABLE,
//                'some_field',
//                true
//            ),
//            ReplyRecord::TABLE,
//            'some_field',
//            true
//        );
//        // Additional commands depending on the db driver
//        switch ($this->driver) {
//            case DbConfig::DRIVER_MYSQL:
//                break;
//            case DbConfig::DRIVER_PGSQL:
//                break;
//        }
    }

    /**
     * Creates the foreign keys needed for the Records used by the plugin
     *
     * @return void
     */
    protected function addForeignKeys()
    {
        // tickit_ticket table
        $this->addForeignKey(
            $this->db->getForeignKeyName(TicketRecord::TABLE, 'id'),
            TicketRecord::TABLE,
            'id',
            '{{%elements}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $this->db->getForeignKeyName(TicketRecord::TABLE, 'userId'),
            TicketRecord::TABLE,
            'userId',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $this->db->getForeignKeyName(TicketRecord::TABLE, 'statusId'),
            TicketRecord::TABLE,
            'statusId',
            StatusRecord::TABLE,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $this->db->getForeignKeyName(TicketRecord::TABLE, 'priorityId'),
            TicketRecord::TABLE,
            'priorityId',
            PriorityRecord::TABLE,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $this->db->getForeignKeyName(TicketRecord::TABLE, 'categoryId'),
            TicketRecord::TABLE,
            'categoryId',
            CategoryRecord::TABLE,
            'id',
            'CASCADE',
            'CASCADE'
        );

        // tickit_question table
        $this->addForeignKey(
            $this->db->getForeignKeyName('{{%tickit_question}}', 'siteId'),
            '{{%tickit_question}}',
            'siteId',
            '{{%sites}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // tickit_template table
        $this->addForeignKey(
            $this->db->getForeignKeyName('{{%tickit_template}}', 'siteId'),
            '{{%tickit_template}}',
            'siteId',
            '{{%sites}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // tickit_reply table
        $this->addForeignKey(
            $this->db->getForeignKeyName(ReplyRecord::TABLE, 'userId'),
            ReplyRecord::TABLE,
            'userId',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $this->db->getForeignKeyName(ReplyRecord::TABLE, 'ticketId'),
            ReplyRecord::TABLE,
            'ticketId',
            TicketRecord::TABLE,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Populates the DB with the default data.
     *
     * @return void
     */
    protected function insertDefaultData()
    {
    }

    /**
     * This method contains the logic to be executed when removing this migration.
     * This method differs from [[down()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[down()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeDown()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        $this->removeTables();

        return true;
    }

    /**
     * Removes the tables needed for the Records used by the plugin
     *
     * @return void
     */
    protected function removeTables()
    {
        // tickit_ticket table
        $this->dropTableIfExists('{{%tickit_ticket}}');

        // tickit_question table
        $this->dropTableIfExists('{{%tickit_question}}');

        // tickit_status table
        $this->dropTableIfExists(StatusRecord::TABLE);

        // tickit_priority table
        $this->dropTableIfExists(PriorityRecord::TABLE);

        // tickit_template table
        $this->dropTableIfExists('{{%tickit_template}}');

        // tickit_reply table
        $this->dropTableIfExists('{{%tickit_reply}}');
    }
}
