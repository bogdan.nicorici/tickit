<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\models;

use teamextension\tickit\Tickit;

use Craft;
use craft\base\Model;

/**
 * ReplyRecord Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class Reply extends Model implements \JsonSerializable
{
    // Public Properties
    // =========================================================================

    public $id;
    public $userId;
    public $message;
    public $ticketId;
    public $dateCreated;
    public $dateUpdated;

    // Public Methods
    // =========================================================================

    public static function create(): Reply
    {
        return new static();
    }

    /**
     * @inheritDoc
     */
    public function safeAttributes(): array
    {
        return [
            'message',
            'userId',
            'ticketId',
        ];
    }

    /**
     * Specify data which should be serialized to JSON
     */
    public function jsonSerialize()
    {
        return [
            'id'            => (int) $this->id,
            'message'       => $this->message,
            'userId'        => (int) $this->userId,
            'ticketId'      => (int) $this->ticketId,
            'dateCreated'   => $this->dateCreated,
            'dateUpdated'   => $this->dateUpdated,
        ];
    }
}
