<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\models;

use craft\base\Model;
use teamextension\tickit\Helpers\ColorHelper;

/**
 * Status Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class Status extends Model implements \JsonSerializable
{
    // Public Properties
    // =========================================================================

    public $id;
    public $handle;
    public $color;
    public $name;
    public $isDefault;
    public $dateCreated;
    public $dateUpdated;

    // Public Methods
    // =========================================================================

    /**
     * @inheritDoc
     */
    public function safeAttributes(): array
    {
        return [
            'name',
            'handle',
            'color',
            'isDefault',
        ];
    }

    public static function create(): Status
    {
        $status = new static();
        $status->color = ColorHelper::randomColor();

        return $status;
    }


    /**
     * Specify data which should be serialized to JSON
     */
    public function jsonSerialize()
    {
        return [
            'id'            => (int) $this->id,
            'name'          => $this->name,
            'handle'        => $this->handle,
            'color'         => $this->color,
            'isDefault'     => $this->isDefault,
            'dateCreated'   => $this->dateCreated,
            'dateUpdated'   => $this->dateUpdated,
        ];
    }
}
