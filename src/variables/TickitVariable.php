<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\variables;

use teamextension\tickit\models\Settings;
use teamextension\tickit\services\CategoriesService;
use teamextension\tickit\services\PrioritiesService;
use teamextension\tickit\services\StatusesService;
use teamextension\tickit\Tickit;

/**
 * tickit Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.tickit }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class TickitVariable
{
    // Public Methods
    // =========================================================================



    public function categories(): CategoriesService
    {
        return Tickit::getInstance()->categories;
    }

    public function statuses(): StatusesService
    {
        return Tickit::getInstance()->statuses;
    }

    public function priorities(): PrioritiesService
    {
        return Tickit::getInstance()->priorities;
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return Tickit::getInstance()->settings->getSettingsModel();
    }

    public function name(): string
    {
        return Tickit::getInstance()->name;
    }
}
