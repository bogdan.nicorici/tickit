<?php
/**
 * Created by PhpStorm.
 * User: bogdan
 * Date: 19.02.2018
 * Time: 12:57 AM
 */

namespace teamextension\tickit\Helpers;


class ColorHelper
{
    public static function randomColor(): string
    {
        return sprintf('#%06X', random_int(0, 0xFFFFFF));
    }
}