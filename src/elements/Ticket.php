<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit\elements;

use craft\helpers\Html;
use teamextension\tickit\elements\Db\TicketQuery;
use teamextension\tickit\models\Priority;
use teamextension\tickit\models\Status;
use teamextension\tickit\Tickit;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQuery;
use craft\elements\db\ElementQueryInterface;
use yii\base\InvalidConfigException;

/**
 * Ticket Element
 *
 * Element is the base class for classes representing elements in terms of objects.
 *
 * @property FieldLayout|null      $fieldLayout           The field layout used by this element
 * @property array                 $htmlAttributes        Any attributes that should be included in the element’s DOM representation in the Control Panel
 * @property int[]                 $supportedSiteIds      The site IDs this element is available in
 * @property string|null           $uriFormat             The URI format used to generate this element’s URL
 * @property string|null           $url                   The element’s full URL
 * @property \Twig_Markup|null     $link                  An anchor pre-filled with this element’s URL and title
 * @property string|null           $ref                   The reference string to this element
 * @property string                $indexHtml             The element index HTML
 * @property bool                  $isEditable            Whether the current user can edit the element
 * @property string|null           $cpEditUrl             The element’s CP edit URL
 * @property string|null           $thumbUrl              The URL to the element’s thumbnail, if there is one
 * @property string|null           $iconUrl               The URL to the element’s icon image, if there is one
 * @property string|null           $status                The element’s status
 * @property Element               $next                  The next element relative to this one, from a given set of criteria
 * @property Element               $prev                  The previous element relative to this one, from a given set of criteria
 * @property Element               $parent                The element’s parent
 * @property mixed                 $route                 The route that should be used when the element’s URI is requested
 * @property int|null              $structureId           The ID of the structure that the element is associated with, if any
 * @property ElementQueryInterface $ancestors             The element’s ancestors
 * @property ElementQueryInterface $descendants           The element’s descendants
 * @property ElementQueryInterface $children              The element’s children
 * @property ElementQueryInterface $siblings              All of the element’s siblings
 * @property Element               $prevSibling           The element’s previous sibling
 * @property Element               $nextSibling           The element’s next sibling
 * @property bool                  $hasDescendants        Whether the element has descendants
 * @property int                   $totalDescendants      The total number of descendants that the element has
 * @property string                $title                 The element’s title
 * @property string|null           $serializedFieldValues Array of the element’s serialized custom field values, indexed by their handles
 * @property array                 $fieldParamNamespace   The namespace used by custom field params on the request
 * @property string                $contentTable          The name of the table this element’s content is stored in
 * @property string                $fieldColumnPrefix     The field column prefix this element’s content uses
 * @property string                $fieldContext          The field context this element’s content uses
 *
 * http://pixelandtonic.com/blog/craft-element-types
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 */
class Ticket extends Element
{
    // Public Properties
    // =========================================================================

    public $categoryId;

    public $statusId;

    public $priorityId;

    // Static Methods
    // =========================================================================

    /**
     * Returns the display name of this class.
     *
     * @return string The display name of this class.
     */
    public static function displayName(): string
    {
        return Tickit::t('Ticket');
    }

    /**
     * @inheritdoc
     */
    public static function hasStatuses(): bool
    {
        return true;
    }

    /**
     * Returns whether elements of this type will be storing any data in the `content`
     * table (tiles or custom fields).
     *
     * @return bool Whether elements of this type will be storing any data in the `content` table.
     */
    public static function hasContent(): bool
    {
        return true;
    }

    /**
     * Returns whether elements of this type have traditional titles.
     *
     * @return bool Whether elements of this type have traditional titles.
     */
    public static function hasTitles(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public static function statuses(): array
    {
        $statuses = Tickit::getInstance()->statuses->getAllStatuses();
        $list = [];
        foreach ($statuses as $status) {
            $list[$status->handle] = ['label' => $status->name, 'color' => $status->color];
        }
        return $list;
    }

    /**
     * Returns whether elements of this type have statuses.
     *
     * If this returns `true`, the element index template will show a Status menu
     * by default, and your elements will get status indicator icons next to them.
     *
     * Use [[statuses()]] to customize which statuses the elements might have.
     *
     * @return bool Whether elements of this type have statuses.
     * @see statuses()
     */
    public static function isLocalized(): bool
    {
        return false;
    }

    /**
     * Creates an [[ElementQueryInterface]] instance for query purpose.
     *
     * The returned [[ElementQueryInterface]] instance can be further customized by calling
     * methods defined in [[ElementQueryInterface]] before `one()` or `all()` is called to return
     * populated [[ElementInterface]] instances. For example,
     *
     * ```php
     * // Find the entry whose ID is 5
     * $entry = Entry::find()->id(5)->one();
     *
     * // Find all assets and order them by their filename:
     * $assets = Asset::find()
     *     ->orderBy('filename')
     *     ->all();
     * ```
     *
     * If you want to define custom criteria parameters for your elements, you can do so by overriding
     * this method and returning a custom query class. For example,
     *
     * ```php
     * class Product extends Element
     * {
     *     public static function find()
     *     {
     *         // use ProductQuery instead of the default ElementQuery
     *         return new ProductQuery(get_called_class());
     *     }
     * }
     * ```
     *
     * You can also set default criteria parameters on the ElementQuery if you don’t have a need for
     * a custom query class. For example,
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->limit(50);
     *     }
     * }
     * ```
     *
     * @return ElementQueryInterface The newly created [[ElementQueryInterface]] instance.
     */
    public static function find(): ElementQueryInterface
    {
        return new TicketQuery(self::class);
    }

    /**
     * Defines the sources that elements of this type may belong to.
     *
     * @param string|null $context The context ('index' or 'modal').
     *
     * @return array The sources.
     * @see sources()
     */
    protected static function defineSources(string $context = null): array
    {
        $sources = [
            [
                'key'      => '*',
                'label'    => Tickit::t('All Tickets'),
                'criteria' => [],
            ],
        ];

        $sources[] = ['heading' => Tickit::t('Priorities')];
        $prioritiesService = Tickit::getInstance()->priorities;
        foreach($prioritiesService->getAllPriorities() as $priority)
        {
            $sources[] = [
                'key'      => 'priority:'.$priority->id,
                'label'    => $priority->name,
                'criteria' => [
                    'priorityId' => $priority->id,
                ],
            ];
        }
//        $formsService = Freeform::getInstance()->forms;
//        /** @var array|null $allowedFormIds */
//        $allowedFormIds = Freeform::getInstance()->submissions->getAllowedSubmissionFormIds();
//        foreach ($formsService->getAllForms() as $form) {
//            if (null !== $allowedFormIds && !\in_array($form->id, $allowedFormIds, true)) {
//                continue;
//            }
//            $sources[] = [
//                'key'      => 'form:' . $form->id,
//                'label'    => $form->name,
//                'criteria' => [
//                    'formId' => $form->id,
//                ],
//            ];
//        }
        return $sources;
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */

    /**
     * @inheritDoc
     */
    protected static function defineTableAttributes(): array
    {
        $titles = [
            'title'         => ['label' => Tickit::t('Subject')],
            'status'        => ['label' => Tickit::t('Status')],
            'priority'      => ['label' => Tickit::t('Priority')],
//            'category'      => ['label' => Tickit::t('Category')],
            'dateCreated'   => ['label' => Tickit::t('Date Created')],
            'dateUpdated'   => ['label' => Tickit::t('Date Updated')],
//            'id'            => ['label' => Tickit::t('ID')],
        ];
//        foreach (Tickit::getInstance()->fields->getAllFieldTickits() as $field) {
//            $titles[self::getFieldColumnName($field->id)] = ['label' => $field->label];
//        }
        return $titles;
    }

    /**
     * @inheritDoc
     */
    protected static function defineDefaultTableAttributes(string $source): array
    {
        return [
            'id',
            'title',
            'status',
            'priority',
//            'category',
            'dateCreated',
            'dateUpdated',
        ];
    }

    /**
     * @inheritDoc
     */
    protected function tableAttributeHtml(string $attribute): string
    {
        if ($attribute === 'status') {
            return $this->getStatusModel()->name;
        }
//        $value = $this->$attribute;
//        if (\is_array($value)) {
//            return Html::decode(implode(', ', $value));
//        }
//        if ($value instanceof AbstractField) {
//            $field = $value;
//            $value = $value->getValue();
//            if (\is_array($value)) {
//                $value = implode(', ', $value);
//            }
//            if ($field instanceof CheckboxField) {
//                return $value ?: '-';
//            }
//            if ($field instanceof RatingField) {
//                return (int) $value . '/' . $field->getMaxValue();
//            }
//            if ($field instanceof FileUploadField) {
//                $asset = \Craft::$app->assets->getAssetById((int) $value);
//                if ($asset) {
//                    return \Craft::$app->view->renderTemplate(
//                        'freeform/_components/fields/file.html',
//                        ['asset' => $asset]
//                    );
//                }
//            }
//            return Html::encode($value);
//        }
        return parent::tableAttributeHtml($attribute);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getStatusModel()->handle;
    }

    /**
     * @return Status|\teamextension\tickit\records\StatusRecord
     */
    public function getStatusModel()
    {
        return Tickit::getInstance()->statuses->getStatusById($this->statusId);
    }

    public function rules()
    {
        return [
            ['someAttribute', 'string'],
            ['someAttribute', 'default', 'value' => 'Some Default'],
        ];
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function getIsEditable(): bool
    {
        return true;
    }

    public function getPriority(): Priority
    {
        return Tickit::getInstance()->priorities->getPriorityById($this->priorityId);
    }

//    /**
//     * Returns the field layout used by this element.
//     *
//     * @return \craft\models\FieldLayout
//     * @throws \yii\base\InvalidConfigException
//     */
//    public function getFieldLayout()
//    {
//        $tagGroup = $this->getGroup();
//
//        if ($tagGroup) {
//            return $tagGroup->getFieldLayout();
//        }
//
//        return null;
//    }

//    /**
//     * @return \craft\models\TagGroup|null
//     * @throws InvalidConfigException
//     */
//    public function getGroup()
//    {
//        if ($this->groupId === null) {
//            throw new InvalidConfigException('Tag is missing its group ID');
//        }
//
//        if (($group = Craft::$app->getTags()->getTagGroupById($this->groupId)) === null) {
//            throw new InvalidConfigException('Invalid tag group ID: '.$this->groupId);
//        }
//
//        return $group;
//    }

    // Indexes, etc.
    // -------------------------------------------------------------------------

    /**
     * Returns the HTML for the element’s editor HUD.
     *
     * @return string The HTML for the editor HUD
     */
    public function getEditorHtml(): string
    {
        $html = Craft::$app->getView()->renderTemplateMacro('_includes/forms', 'textField', [
            [
                'label' => Craft::t('app', 'Title'),
                'siteId' => $this->siteId,
                'id' => 'title',
                'name' => 'title',
                'value' => $this->title,
                'errors' => $this->getErrors('title'),
                'first' => true,
                'autofocus' => true,
                'required' => true
            ]
        ]);

        $html .= parent::getEditorHtml();

        return $html;
    }

    // Events
    // -------------------------------------------------------------------------

    /**
     * Performs actions before an element is saved.
     *
     * @param bool $isNew Whether the element is brand new
     *
     * @return bool Whether the element should be saved
     */
    public function beforeSave(bool $isNew): bool
    {
        return true;
    }

    /**
     * Performs actions after an element is saved.
     *
     * @param bool $isNew Whether the element is brand new
     *
     * @return void
     */
    public function afterSave(bool $isNew)
    {
    }

    /**
     * Performs actions before an element is deleted.
     *
     * @return bool Whether the element should be deleted
     */
    public function beforeDelete(): bool
    {
        return true;
    }

    /**
     * Performs actions after an element is deleted.
     *
     * @return void
     */
    public function afterDelete()
    {
    }

    /**
     * Performs actions before an element is moved within a structure.
     *
     * @param int $structureId The structure ID
     *
     * @return bool Whether the element should be moved within the structure
     */
    public function beforeMoveInStructure(int $structureId): bool
    {
        return true;
    }

    /**
     * Performs actions after an element is moved within a structure.
     *
     * @param int $structureId The structure ID
     *
     * @return void
     */
    public function afterMoveInStructure(int $structureId)
    {
    }
}
