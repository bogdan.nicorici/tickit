<?php

namespace teamextension\tickit\elements\Db;

use craft\elements\db\ElementQuery;
use craft\helpers\Db;
use teamextension\tickit\elements\Ticket;
use teamextension\tickit\records\CategoryRecord;
use teamextension\tickit\records\PriorityRecord;
use teamextension\tickit\records\StatusRecord;
use teamextension\tickit\records\TicketRecord;

class TicketQuery extends ElementQuery
{
    /** @var int */
    public $categoryId;
    /** @var string */
    public $category;
    /** @var int */
    public $priorityId;
    /** @var string */
    public $priority;
    /** @var int */
    public $statusId;

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function categoryId($value): TicketQuery
    {
        $this->categoryId = $value;
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function category(string $value): TicketQuery
    {
        $this->category = $value;
        return $this;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function priorityId($value): TicketQuery
    {
        $this->priorityId = $value;
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function priority(string $value): TicketQuery
    {
        $this->priority = $value;
        return $this;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function statusId($value): TicketQuery
    {
        $this->statusId = (int)$value;
        return $this;
    }

    /**
     * @return bool
     */
    protected function beforePrepare(): bool
    {
        $table = TicketRecord::TABLE_STD;
        $priorityTable = PriorityRecord::TABLE_STD;
        $statusTable = StatusRecord::TABLE_STD;
        $this->joinElementTable($table);
        $hasPriorityJoin = false;
        if (\is_array($this->join)) {
            foreach ($this->join as $joinData) {
                if (isset($joinData[1]) && $joinData[1] === PriorityRecord::TABLE . ' ' . $priorityTable) {
                    $hasPriorityJoin = true;
                }
            }
        }
        if (!$hasPriorityJoin) {
            $this->innerJoin(PriorityRecord::TABLE . ' ' . $priorityTable, "`$priorityTable`.`id` = `$table`.`priorityId`");
            $this->innerJoin(StatusRecord::TABLE . ' ' . $statusTable, "`$statusTable`.`id` = `$table`.`statusId`");
//            $this->innerJoin(CategoryRecord::TABLE . ' ' . $statusTable, "`$statusTable`.`id` = `$table`.`categoryId`");
        }
        $select = [
            $table . '.priorityId',
            $table . '.statusId',
            //$table . '.categoryId',
        ];

        $this->query->select($select);
        if ($this->priorityId) {
            $this->subQuery->andWhere(Db::parseParam($table . '.priorityId', $this->priorityId));
        }
        if ($this->priority) {
            $this->subQuery->andWhere(Db::parseParam($priorityTable . '.handle', $this->priority));
        }
        if ($this->statusId) {
            $this->subQuery->andWhere(Db::parseParam($table . '.statusId', $this->statusId));
        }
        if ($this->status && \is_string($this->status)) {
            $this->subQuery->andWhere(Db::parseParam($statusTable . '.handle', $this->status));
            $this->status = '';
        }
        $customSortTables = [
            'status' => "$statusTable.name",
            'priority' => "$priorityTable.name",
        ];
        foreach ($customSortTables as $column => $columnUpdate) {
            if (isset($this->orderBy[$column])) {
                $sortOrder = $this->orderBy[$column];
                unset($this->orderBy[$column]);
                $this->orderBy([$columnUpdate => $sortOrder]);
            }
        }
        if (!empty($this->orderBy) && \is_array($this->orderBy)) {
            $orderExceptions = ['title'];
            $prefixedOrderList = [];
            foreach ($this->orderBy as $key => $sortDirection) {
                if (\in_array($key, $orderExceptions, true) || preg_match('/^[a-z0-9_]+\./i', $key)) {
                    $prefixedOrderList[$key] = $sortDirection;
                    continue;
                }
                $prefixedOrderList[$table . '.' . $key] = $sortDirection;
            }
            $this->orderBy = $prefixedOrderList;
        }
        return parent::beforePrepare();
    }
}