<?php
/**
 * tickit plugin for Craft CMS 3.x
 *
 * Ticketing support system
 *
 * @link      http://teamextension.ro
 * @copyright Copyright (c) 2018 Bogdan Nicorici @ TeamExtension
 */

namespace teamextension\tickit;

use teamextension\tickit\services\CategoriesService;
use teamextension\tickit\services\TicketsService as TicketsService;
use teamextension\tickit\services\QuestionsService as QuestionsService;
use teamextension\tickit\services\StatusesService as StatusesService;
use teamextension\tickit\services\PrioritiesService as PrioritiesService;
use teamextension\tickit\services\TemplatesService as TemplatesService;
use teamextension\tickit\services\RepliesService as RepliesService;
use teamextension\tickit\services\ResponsesService as ResponsesService;
use teamextension\tickit\variables\TickitVariable;
use teamextension\tickit\models\Settings;
use teamextension\tickit\elements\Ticket as TicketElement;
use teamextension\tickit\elements\Question as QuestionElement;
use teamextension\tickit\elements\Reply as ReplyElement;
use teamextension\tickit\elements\Status as StatusElement;
use teamextension\tickit\elements\Priority as PriorityElement;
use teamextension\tickit\elements\Template as TemplateElement;
use teamextension\tickit\elements\Response as ResponseElement;
use teamextension\tickit\widgets\Tickets as TicketsWidget;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Bogdan Nicorici @ TeamExtension
 * @package   Tickit
 * @since     1.0.0
 *
 * @property  TicketsService $tickets
 * @property  QuestionsService $questions
 * @property  StatusesService $statuses
 * @property  CategoriesService $categories
 * @property  PrioritiesService $priorities
 * @property  TemplatesService $templates
 * @property  RepliesService $replies
 * @property  ResponsesService $responses
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class Tickit extends Plugin
{
    const TRANSLATION_CATEGORY = 'tickit';

    public $hasCpSection = true;

    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * Tickit::$plugin
     *
     * @var Tickit
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * Tickit::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'tickit/tickets';
                $event->rules['siteActionTrigger2'] = 'tickit/statuses';
                $event->rules['siteActionTrigger3'] = 'tickit/priorities';
                $event->rules['siteActionTrigger4'] = 'tickit/questions';
                $event->rules['siteActionTrigger5'] = 'tickit/replies';
                $event->rules['siteActionTrigger6'] = 'tickit/templates';
                $event->rules['siteActionTrigger7'] = 'tickit/responses';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $routes       = include __DIR__ . '/routes.php';
                $event->rules = array_merge($event->rules, $routes);
//                $event->rules['cpActionTrigger1'] = 'tickit/tickets/do-something';
//                $event->rules['cpActionTrigger2'] = 'tickit/statuses/do-something';
//                $event->rules['cpActionTrigger3'] = 'tickit/priorities/do-something';
//                $event->rules['cpActionTrigger4'] = 'tickit/questions/do-something';
//                $event->rules['cpActionTrigger5'] = 'tickit/replies/do-something';
//                $event->rules['cpActionTrigger6'] = 'tickit/templates/do-something';
//                $event->rules['cpActionTrigger7'] = 'tickit/responses/do-something';
            }
        );

//        // Register our fields
//        Event::on(
//            Fields::class,
//            Fields::EVENT_REGISTER_FIELD_TYPES,
//            function (RegisterComponentTypesEvent $event) {
//                $event->types[] = StatusField::class;
//                $event->types[] = PriorityField::class;
//            }
//        );

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = TicketElement::class;
                $event->types[] = QuestionElement::class;
                $event->types[] = ReplyElement::class;
                $event->types[] = StatusElement::class;
                $event->types[] = PriorityElement::class;
                $event->types[] = TemplateElement::class;
                $event->types[] = ResponseElement::class;
            }
        );

        // Register our widgets
        Event::on(
            Dashboard::class,
            Dashboard::EVENT_REGISTER_WIDGET_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = TicketsWidget::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('tickit', TickitVariable::class);
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'tickit',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'tickit/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }

    public function getCpNavItem()
    {
        $item = parent::getCpNavItem();
        $item['subnav'] = [
            'foo' => ['label' => 'Foo', 'url' => 'plugin-handle/foo'],
            'bar' => ['label' => 'Bar', 'url' => 'plugin-handle/bar'],
            'baz' => ['label' => 'Baz', 'url' => 'plugin-handle/baz'],
        ];
        return $item;
    }

    /**
     * @param string $message
     * @param array  $params
     * @param string $language
     *
     * @return string
     */
    public static function t(string $message, array $params = [], string $language = null): string
    {
        return \Craft::t(self::TRANSLATION_CATEGORY, $message, $params, $language);
    }
}
