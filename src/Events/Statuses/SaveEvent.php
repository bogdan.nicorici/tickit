<?php
namespace teamextension\tickit\Events\Statuses;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Status;

class SaveEvent extends CancelableEvent
{
    /** @var Status */
    private $model;
    /** @var bool */
    private $new;
    /**
     * @param Status $status
     * @param bool        $new
     */
    public function __construct(Status $status, bool $new)
    {
        $this->model = $status;
        $this->new   = $new;
        parent::__construct();
    }
    /**
     * @return Status
     */
    public function getModel(): Status
    {
        return $this->model;
    }
    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->new;
    }
}