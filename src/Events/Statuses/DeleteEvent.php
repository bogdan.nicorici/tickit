<?php
namespace teamextension\tickit\Events\Statuses;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Status;

class DeleteEvent extends CancelableEvent
{
    /** @var Status */
    private $model;
    /**
     * @param Status $model
     */
    public function __construct(Status $model)
    {
        $this->model = $model;
        parent::__construct();
    }
    /**
     * @return Status
     */
    public function getModel(): Status
    {
        return $this->model;
    }
}