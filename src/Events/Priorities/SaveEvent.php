<?php
namespace teamextension\tickit\Events\Priorities;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Priority;

class SaveEvent extends CancelableEvent
{
    /** @var Priority */
    private $model;
    /** @var bool */
    private $new;
    /**
     * @param Priority $priority
     * @param bool        $new
     */
    public function __construct(Priority $priority, bool $new)
    {
        $this->model = $priority;
        $this->new   = $new;
        parent::__construct();
    }
    /**
     * @return Priority
     */
    public function getModel(): Priority
    {
        return $this->model;
    }
    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->new;
    }
}