<?php
namespace teamextension\tickit\Events\Priorities;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Priority;

class DeleteEvent extends CancelableEvent
{
    /** @var Priority */
    private $model;
    /**
     * @param Priority $model
     */
    public function __construct(Priority $model)
    {
        $this->model = $model;
        parent::__construct();
    }
    /**
     * @return Priority
     */
    public function getModel(): Priority
    {
        return $this->model;
    }
}