<?php
namespace teamextension\tickit\Events\Categories;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Category;

class SaveEvent extends CancelableEvent
{
    /** @var Category */
    private $model;
    /** @var bool */
    private $new;
    /**
     * @param Category $category
     * @param bool        $new
     */
    public function __construct(Category $category, bool $new)
    {
        $this->model = $category;
        $this->new   = $new;
        parent::__construct();
    }
    /**
     * @return Category
     */
    public function getModel(): Category
    {
        return $this->model;
    }
    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->new;
    }
}