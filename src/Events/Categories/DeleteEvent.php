<?php
namespace teamextension\tickit\Events\Categories;
use craft\events\CancelableEvent;
use teamextension\tickit\models\Category;

class DeleteEvent extends CancelableEvent
{
    /** @var Category */
    private $model;
    /**
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
        parent::__construct();
    }
    /**
     * @return Category
     */
    public function getModel(): Category
    {
        return $this->model;
    }
}