<?php
/**
 * Created by PhpStorm.
 * User: bogdan
 * Date: 19.02.2018
 * Time: 12:29 AM
 */

return [
    //Categories
    'tickit/categories'                                 => 'tickit/categories/index',
    'tickit/categories/new'                             => 'tickit/categories/create',
    'tickit/categories/<id:\d+>'                        => 'tickit/categories/edit',
    'tickit/categories/save'                            => 'tickit/categories/save',
    'tickit/categories/delete'                          => 'tickit/categories/delete',

    //Statuses
    'tickit/statuses'                                 => 'tickit/statuses/index',
    'tickit/statuses/new'                             => 'tickit/statuses/create',
    'tickit/statuses/<id:\d+>'                        => 'tickit/statuses/edit',
    'tickit/statuses/save'                            => 'tickit/statuses/save',
    'tickit/statuses/delete'                          => 'tickit/statuses/delete',

    //Statuses
    'tickit/priorities'                                 => 'tickit/priorities/index',
    'tickit/priorities/new'                             => 'tickit/priorities/create',
    'tickit/priorities/<id:\d+>'                        => 'tickit/priorities/edit',
    'tickit/priorities/save'                            => 'tickit/priorities/save',
    'tickit/priorities/delete'                          => 'tickit/priorities/delete',

    //Statuses
    'tickit/tickets'                                 => 'tickit/tickets/index',
    'tickit/tickets/new'                             => 'tickit/tickets/create',
    'tickit/tickets/<id:\d+>'                        => 'tickit/tickets/edit',
    'tickit/tickets/save'                            => 'tickit/tickets/save',
    'tickit/tickets/delete'                          => 'tickit/tickets/delete',
];